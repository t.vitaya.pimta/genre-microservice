package com.example.genre.services;

import com.example.genre.exception.EntityNotFound;
import com.example.genre.models.Genre;
import com.example.genre.repository.JdbcGenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

@Component
public class GenreService {
    @Autowired
    JdbcGenreRepository genreRepository;

    public Genre getGenre(String genreId) throws EntityNotFound {
        try {
            return this.genreRepository.getById(genreId);
        }
        catch (DataAccessException e) {
            throw new EntityNotFound("genre not found");
        }
    }

    public boolean isGenreIdExist(String genreId) {
        if(this.genreRepository.countGenreById(genreId) == 1) {
            return true;
        }
        return false;
    }
}
