package com.example.genre.exception;

public class EntityNotFound extends Exception{
    public EntityNotFound(String errMessage) {
        super(errMessage);
    }
}
