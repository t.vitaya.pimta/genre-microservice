package com.example.genre.dto;

import lombok.Getter;

@Getter
public class ResponseObj<T> {
    private Status status;
    private T data;

    public ResponseObj(int code, String description, T data) {
        this(code, description);
        this.data = data;
    }

    public ResponseObj(int code, String description) {
        this.status = new Status(code, description);
    }
}
