package com.example.genre.dto;

import lombok.Getter;

@Getter
public class Status {
    private int code;
    private String description;

    public Status(int code, String description) {
        this.code = code;
        this.description = description;
    }
}
