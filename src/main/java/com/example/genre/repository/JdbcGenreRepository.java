package com.example.genre.repository;

import com.example.genre.models.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class JdbcGenreRepository implements GenreRepository{

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public Genre getById(String id) throws DataAccessException {
        return jdbcTemplate.queryForObject(
                "select * from genre where uuid = ?",
                new Object[]{id},
                (rs, rowNum) ->
                        new Genre(
                                rs.getString("uuid"),
                                rs.getString("name"),
                                rs.getTimestamp("createdTime"),
                                rs.getTimestamp("updatedTime")
                        )
        );
    }

    @Override
    public int countGenreById(String id) {
        return jdbcTemplate.queryForObject(
                "select count(*) from genre where uuid = ?",
                new Object[]{id},
                Integer.class
        );
    }
}
