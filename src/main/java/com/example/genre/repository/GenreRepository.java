package com.example.genre.repository;

import com.example.genre.models.Genre;

public interface GenreRepository {
    Genre getById(String id);
    int countGenreById(String id);
}
