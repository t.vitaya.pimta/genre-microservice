package com.example.genre.controllers;

import com.example.genre.dto.ResponseObj;
import com.example.genre.exception.EntityNotFound;
import com.example.genre.models.Genre;
import com.example.genre.services.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GenreController {

    @Autowired
    GenreService genreService;

    @GetMapping("/{genreId}")
    public ResponseEntity<ResponseObj<Genre>> getGenre(@PathVariable String genreId) {
        try {
            Genre genre = this.genreService.getGenre(genreId);
            return ResponseEntity.ok(new ResponseObj<>(200, "OK", genre));
        }
        catch (EntityNotFound e) {
            return ResponseEntity.ok(new ResponseObj<>(3000, e.getMessage()));
        }
    }
}
