package com.example.genre.models;

import lombok.Getter;

import java.sql.Timestamp;

@Getter
public class Genre {
    private String uuid;
    private String name;
    private Timestamp createdTime;
    private Timestamp updatedTime;

    public Genre(String uuid, String name, Timestamp createdTime, Timestamp updatedTime) {
        this.uuid = uuid;
        this.name = name;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
    }
}
